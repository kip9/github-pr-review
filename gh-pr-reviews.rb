require 'bundler/setup'
require 'Octokit'
require 'pp'
require 'ostruct'
require 'optparse'
require 'optparse/time'

PER_PAGE = 100
MODE_PULL_REQUEST_COUNT = "pull_request_count"
MODE_DATE = "date"

options = OpenStruct.new
options.mode = MODE_PULL_REQUEST_COUNT
options.pullRequestsCount=100
options.startDate = nil
options.endDate = nil
options.repository='formstack/web'

OptionParser.new do |opts|
	opts.banner = "Usage: gh-pr-reviews.rb [options]"

	opts.on("-r", "--repository REPOSITORY", "Github repository to check") do |repo|
		options[:repository] = repo
	end

	opts.on("-c", "--count COUNT", "Number of Pull Requests to check", OptionParser::DecimalInteger) do |count|
		options[:mode] = MODE_PULL_REQUEST_COUNT
		options[:pullRequestsCount] = count
	end

	opts.on("-f", "--from DATE", Time, "Starting date") do |time|
		options.mode = MODE_DATE
		options.startDate = time
	end

	opts.on("-t", "--to DATE", Time, "Ending date") do |time|
		options.mode = MODE_DATE
		options.endDate = time
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end
end.parse!

# See for details: https://github.com/octokit/octokit.rb#using-a-netrc-file
client = Octokit::Client.new(:netrc => true)
client.auto_paginate = false

user = client.user

results = {}
total_reviews = 0

closed_pull_requests = {}

if options.mode == MODE_PULL_REQUEST_COUNT
	pagesToFetch = (options.pullRequestsCount.to_f / PER_PAGE.to_f).ceil
	current_page = 1

	closed_pull_requests = client.pull_requests(options.repository, :state => 'closed', :per_page => PER_PAGE)

	while current_page < pagesToFetch do
		closed_pull_requests.concat client.get client.last_response.rels[:next].href
		current_page += 1
	end
	pull_request_numbers = closed_pull_requests.take(options.pullRequestsCount).map { |element| element.number }
else
		date_search_string = "created:>=#{options.startDate.strftime("%F")}"
		if options.endDate
			date_search_string = "created:#{options.startDate.strftime("%F")}..#{options.endDate.strftime("%F")}"
		end
		search_string = "#{date_search_string} type:pr is:closed repo:#{options.repository}"
		client.auto_paginate = true
		closed_pull_requests = client.search_issues(search_string, :sort => 'created', :order => 'desc', :per_page => PER_PAGE)[:items]
		pull_request_numbers = closed_pull_requests.map { |element| element.number }
end

puts "#{pull_request_numbers.count} pull requests in repository #{options.repository} found"
puts "Retrieving PR reviews ..."

counter = 0
pull_request_numbers.each { |number|
	reviews = client.pull_request_reviews(options.repository, number, :accept => Octokit::Preview::PREVIEW_TYPES[:reviews])
	reviews.each { |review|
		if results[review.user.login].nil? then
			results[review.user.login] = 0
		end
		results[review.user.login] += 1
		total_reviews += 1
	}
	counter += 1
	if counter % 10 == 0 then
		puts "#{counter} PR's processed"
	end
}

puts "PRs checked: #{pull_request_numbers.count}"
puts "First PR on #{closed_pull_requests[-1].created_at}"
puts "Total reviews: #{total_reviews}"

pp results.sort_by { |name, count| count }.reverse
